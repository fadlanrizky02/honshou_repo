package mvcdemo.controllers;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvcdemo.model.Authenticator;
import mvcdemo.model.Koneksi;
import mvcdemo.model.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import sun.text.normalizer.ICUBinary.Authenticate;

public class LoginController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public LoginController() {
        super();
    }
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        String email = request.getParameter("email");
        String password = request.getParameter("password");

        Authenticator authenticator = new Authenticator();
        String result = authenticator.authenticate(email, password);
        if (result.equals("success")) {
            response.sendRedirect("/servletMVC/LoginController");
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("/error.jsp");
            rd.forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Koneksi conn = new Koneksi();
        Connection c = conn.Koneksi();
        RequestDispatcher rd = req.getRequestDispatcher("/success.jsp");
        try {
            Statement statement = c.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM user");
            ArrayList<User> usr = new ArrayList<User>();
            while (resultSet.next()) {
                User user = new User(resultSet.getInt("id_user"), resultSet.getString("fullname"), resultSet.getString("email"), resultSet.getString("password"), resultSet.getString("address"), resultSet.getString("status"), resultSet.getInt("physics"), resultSet.getInt("calculus"), resultSet.getInt("biologi"));
                usr.add(user);
            }
            req.setAttribute("data", usr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        rd.forward(req, res);
    }
}
