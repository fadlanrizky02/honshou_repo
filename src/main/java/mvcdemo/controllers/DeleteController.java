package mvcdemo.controllers;

import mvcdemo.model.Koneksi;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;

public class DeleteController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Koneksi conn = new Koneksi();
        Connection c = conn.Koneksi();
        int id = Integer.parseInt(req.getParameter("id"));
        PrintWriter pw = resp.getWriter();
        try {
            Statement statement = c.createStatement();
            statement.execute("DELETE FROM user WHERE id_user='"+id+"'");
            resp.sendRedirect("/servletMVC/LoginController");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
